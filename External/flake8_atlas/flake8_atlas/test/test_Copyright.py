# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from __future__ import print_function

from flake8_atlas.checks import Copyright
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test Copyright checker
   """
   def test_fileLimit(self):
      f = open('test.py', 'w+')
      print('#', file=f)
      self.assertPass(f, Copyright)

   def test_noCopyright(self):
      f = open('test.py', 'w+')
      print('#'*300, file=f)    # to get above min file size
      self.assertFail(f, Copyright)

   def test_correctCopyright(self):
      f = open('test.py', 'w+')
      print('#', file=f)
      print('# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration', file=f)
      print('#'*300, file=f)
      self.assertPass(f, Copyright)

   def test_wrongCopyright(self):
      f = open('test.py', 'w+')
      print('#', file=f)
      print('# Copyright (C) 2002-2019 Jane Doe', file=f)
      print('#'*300, file=f)
      self.assertFail(f, Copyright)
