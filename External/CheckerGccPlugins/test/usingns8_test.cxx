// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

namespace std {}

class ChainGroup
{
public:
  template<class CONTAINER>
  void  features(  ) const;
};

#include "usingns8_test.h"

using namespace std;

void fill (const ChainGroup* cg)
{
  cg->features<int>( );
}
