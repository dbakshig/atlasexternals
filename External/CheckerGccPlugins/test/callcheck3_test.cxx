// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
// Test callcheck for misc kernel classes.


namespace std
{
template<typename> class allocator;
template<class _CharT>  struct char_traits;
template<> struct char_traits<char>;
template<typename _CharT, typename _Traits = char_traits<_CharT>,
         typename _Alloc = allocator<_CharT> >
  class basic_string;
typedef basic_string<char>    string;   
} // namespace


namespace CLHEP
{
class HepRandomEngine {};
}


class EventContext {};


class ICoreDumpSvc
{
public:
  virtual void setCoreDumpInfo( const std::string& name, const std::string& value ) = 0;
  virtual void setCoreDumpInfo( const EventContext& ctx, const std::string& name, const std::string& value ) = 0;
};


class IDecisionSvc
{
public:
  virtual bool isEventAccepted(const std::string& stream) const = 0;
  virtual bool isEventAccepted(const std::string& stream, const EventContext&) const = 0;
};


//*************************************************************************


namespace SG {


template <class T>
class SlotSpecificObj
{
public:
  T* get (const EventContext& ctx);
  T* get();
  const T* get (const EventContext& ctx) const;
  const T* get() const;
};


}


//*************************************************************************


namespace Athena
{

template <class T> class RCUReadQuiesce;
template <class T> class RCUUpdate;

class IRCUObject
{
public:
  void quiescent();
  void quiescent (const EventContext& ctx);
};


template <class T>
class RCUObject
  : public IRCUObject
{
public:
  typedef RCUReadQuiesce<T> ReadQuiesce_t;
  typedef RCUUpdate<T> Update_t;

  ReadQuiesce_t readerQuiesce();
  ReadQuiesce_t readerQuiesce (const EventContext& ctx);

  Update_t updater();
  Update_t updater (const EventContext& ctx);
};


template <class T>
class RCUReadQuiesce
{
public:
  RCUReadQuiesce (RCUObject<T>& rcuobj);
  RCUReadQuiesce (RCUObject<T>& rcuobj, const EventContext& ctx);
};


template <class T>
class RCUUpdate
{
public:
  RCUUpdate (RCUObject<T>& rcuobj);
  RCUUpdate (RCUObject<T>& rcuobj, const EventContext& ctx);
};


//*************************************************************************


class Timeout
{
public:
  static Timeout& instance();
  static Timeout& instance(const EventContext& ctx);
};


}


//*************************************************************************


namespace ATHRNG
{


class RNGWrapper
{
public:
  operator CLHEP::HepRandomEngine*() const;
  CLHEP::HepRandomEngine* getEngine(const EventContext &ctx) const;
};


}


//*************************************************************************


void testCoreDumpSvc (ICoreDumpSvc* svc,
                      const std::string& name,
                      const std::string& value,
                      const EventContext& xyzzy)
{
  svc->setCoreDumpInfo (xyzzy, name, value);
  svc->setCoreDumpInfo (name, value);
}


void testDecisionSvc (IDecisionSvc* svc, const std::string& stream,
                      const EventContext& xyzzy)
{
  svc->isEventAccepted (stream);
  svc->isEventAccepted (stream, xyzzy);
}


void testRCUObject (Athena::RCUObject<int>& robj,
                    const EventContext& xyzzy)
{
  robj.quiescent(xyzzy);
  robj.quiescent();

  robj.readerQuiesce();
  robj.updater();

  Athena::RCUReadQuiesce<int> rq (robj);
  Athena::RCUUpdate<int> uq (robj);
}


void testRNGWrapper (const ATHRNG::RNGWrapper& w,
                     const EventContext& xyzzy)
{
  w.getEngine (xyzzy);
  CLHEP::HepRandomEngine* e = w;
}


void testSlotSpecificObj (SG::SlotSpecificObj<int>& obj,
                          const EventContext& xyzzy)
{
  obj.get(xyzzy);
  obj.get();
  const SG::SlotSpecificObj<int>& obj_c = obj;
  obj_c.get();
}


void testTimeout (const EventContext& xyzzy)
{
  Athena::Timeout::instance(xyzzy);
  Athena::Timeout::instance();
}
