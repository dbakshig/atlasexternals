# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building the FFTW library as part of the offline software build.
#

# Set the name of the package:
atlas_subdir( FFTW )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Decide whether to request debug symbols from the build:
set( _fftwExtraConfig )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   set( _fftwExtraConfig --enable-debug )
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FFTWBuild )

# Set up the build of FFTW for the build area:
ExternalProject_Add( FFTW
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/FFTW/fftw-3.3.3.tar.gz
   URL_MD5 0a05ca9c7b3bfddc8278e7c40791a1c2
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   PATCH_COMMAND patch -p0 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/fftw-3.3.3.patch
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${_buildDir}
   --enable-shared --enable-float ${_fftwExtraConfig}
   BUILD_COMMAND make
   COMMAND make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( FFTW purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for FFTW"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_FFTW FFTW )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
