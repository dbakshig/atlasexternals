# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#   PYXML_PYTHON_PATH
#   PYXML_BINARY_PATH
#
# Can be steered by PYXML_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME pyxml
   PYTHON_NAMES _xmlplus.py _xmlplus/__init__.py
   BINARY_NAMES xmlproc_parse xmlproc_val
   BINARY_SUFFIXES bin )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pyxml DEFAULT_MSG
   _PYXML_PYTHON_PATH _PYXML_BINARY_PATH )

# Set up the RPM dependency.
lcg_need_rpm( pyxml )
